const CONTAINER = $('#container');
const apiTrending = 'http://api.giphy.com/v1/gifs/trending?';
const apiSearch = 'http://api.giphy.com/v1/gifs/search?';
const apiKey = '&api_key=COx0OxszCvqVz8PuOGeTUSYk9MOniUj5';
const limit = '&limit=20';
const api = 'http://api.giphy.com/v1/gifs?'
const apiRandom = 'http://api.giphy.com/v1/gifs/random?'
const apiUp = 'https://upload.giphy.com/v1/gifs?'
const upKey = 'api_key=COx0OxszCvqVz8PuOGeTUSYk9MOniUj5'

const myForm = document.getElementById('myForm');
const inpFile = document.getElementById('myFile');

export {
  CONTAINER,
  apiTrending,
  apiSearch,
  apiKey,
  limit,
  api,
  apiRandom,
  myForm,
  inpFile,
  apiUp,
  upKey
}


