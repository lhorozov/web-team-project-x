import { myForm, upKey } from './common.js'

export const gifInfoOver = (callback) => {
  return $(document).on('mouseenter', 'img', callback)
}

export const uploadGifs = (callback) => {
  return $(myForm).on('submit', callback)
}

export const gifInfoLeave = (callback) => {
  return $(document).on('mouseleave', 'img', callback)
}

export const infiniteScroll = (callback) => {
  $(window).scroll(callback);
};


export const trendingBTN = (callback) => {
  $(document).on('click', '#trend-btn', callback);
}

export const searchBTN = (callback) => {
  $(document).on('click', '#search-button', callback);
}

export const favoriteBTNheart = (callback) => {
  $(document).on('click', '.bi-heart-fill', callback);
}

export const favoriteBTNcontainer = (callback) => {
  $(document).on('click', '#show-fav', callback);
}

export const favoriteClear = (callback) => {
  $(document).on('click', '#clear-fav', callback);
}

export const searchEnter = (callback) => {
  $(document).on('keypress', '#search-bar', function(e) {
    if (e.which === 13) {
      callback();
    }
  });
}
